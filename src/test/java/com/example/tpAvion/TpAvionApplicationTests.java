package com.example.tpAvion;

import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.tpAvion.exception.NotFoundException;
import com.example.tpAvion.model.User;
import com.example.tpAvion.repository.UserRepository;
import com.example.tpAvion.service.UserService;

@SpringBootTest
class TpAvionApplicationTests {

	@Autowired
	UserRepository userRepo;

	@Autowired
	private UserService userServ;

	@Test
	void contextLoads() {
	}

	@Test
	void login() throws UnsupportedEncodingException, NotFoundException {

		User u = userRepo.findByEmailAndMotDePasse("jean@gmail.com", "jean");
		System.out.println(u);
		System.out.println(userServ.login("jean@gmail.com", "jean").getToken());
		// System.out.println(u);
	}

	@Test
	void loginRepo() throws UnsupportedEncodingException, NotFoundException {

		// User u = userRepo.findByEmailAndMotDePasse("aina", "aina");
		System.out.println(userServ.login("aina", "aina"));
		// System.out.println(u);
	}
}
