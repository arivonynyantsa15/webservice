-- CREATE USER avion WITH PASSWORD 'avion';
-- CREATE DATABASE avion OWNER avion;

CREATE TABLE avion
(
    idAvion bigint NOT NULL,
    idmarque bigint NOT NULL,
    nImmatriculation character varying(10) NOT NULL,
    imageavion text,
    format character varying(10),
    PRIMARY KEY (idAvion)
);


ALTER TABLE IF EXISTS Avion
    ALTER COLUMN idAvion ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 );

CREATE TABLE Marque
(
    idMarque bigint NOT NULL,
    nom character varying(30) NOT NULL,
    PRIMARY KEY (idMarque)
);

ALTER TABLE IF EXISTS Marque
    ALTER COLUMN idMarque ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 );

CREATE TABLE users
(
    idUser bigint NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    motDepasse character varying(256) NOT NULL,
    email character varying(100) NOT NULL,
    PRIMARY KEY (idUser)
);

ALTER TABLE IF EXISTS Users
    ALTER COLUMN idUser ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 );

CREATE TABLE Connexion
(
    idConnexion bigint NOT NULL,
    idUser bigint NOT NULL,
    token character varying(256) NOT NULL,
    expiration timestamp NOT NULL,
    PRIMARY KEY (idConnexion)
);


ALTER TABLE IF EXISTS Connexion
    ALTER COLUMN idConnexion ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 );

ALTER TABLE IF EXISTS Connexion
    ADD CONSTRAINT fk_conn_user FOREIGN KEY (idUser)
    REFERENCES Users (idUser) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

CREATE TABLE Trajet
(
    idTrajet bigint NOT NULL,
    idAvion bigint NOT NULL,
    dateInsert date NOT NULL,
    debutKm integer NOT NULL,
    finKm integer NOT NULL,
    PRIMARY KEY (idTrajet)
);


ALTER TABLE IF EXISTS Trajet
    ALTER COLUMN idTrajet ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 );

ALTER TABLE IF EXISTS Trajet
    ADD CONSTRAINT fk_traj_vehi FOREIGN KEY (idAvion)
    REFERENCES Avion (idAvion) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE Avion ADD FOREIGN KEY (idMarque) REFERENCES Marque(idmarque);



INSERT INTO Marque (nom)
    VALUES ('Volskwagen');

INSERT INTO Marque (nom)
    VALUES ('Mercedes');

INSERT INTO Marque (nom)
    VALUES ('Renault');

INSERT INTO Marque (nom)
    VALUES ('Audi');

INSERT INTO Marque (nom)
    VALUES ('Hyundai');

INSERT INTO Avion(idmarque, nImmatriculation)
	VALUES (1, '0000');

INSERT INTO Avion(idmarque, nImmatriculation)
	VALUES (2, '0001');

INSERT INTO Avion(idmarque, nImmatriculation)
	VALUES (3, '0002');

INSERT INTO Avion(idmarque, nImmatriculation)
	VALUES (4, '0003');

INSERT INTO Avion(idmarque, nImmatriculation)
	VALUES (5, '0004');

INSERT INTO Avion(idmarque, nImmatriculation)
	VALUES (1, '6969');

INSERT INTO Trajet(idAvion, dateInsert, debutKm, finKm)
	VALUES (1, '2022-04-06', 10, 20);

INSERT INTO Trajet(idAvion, dateInsert, debutKm, finKm)
	VALUES (2, '2022-05-07', 20, 30);

INSERT INTO Trajet(idAvion, dateInsert, debutKm, finKm)
	VALUES (3, '2022-06-08', 30, 40);

INSERT INTO Trajet(idAvion, dateInsert, debutKm, finKm)
	VALUES (4, '2022-07-09', 40, 50);

INSERT INTO Trajet(idAvion, dateInsert, debutKm, finKm)
	VALUES (5, '2022-10-11', 50, 60);
-- Mdp: "MotDePasse" 

INSERT INTO Users(nom, prenom, motDepasse, email)
	VALUES 
    ('Jean','Rakoto', md5('jean'), 'jean@gmail.com');

INSERT INTO Users(nom, prenom, motDepasse, email)
	VALUES 
    ('Jean','Rakoto', md5('aina'), 'aina');

CREATE TABLE ASSURANCE (
    id serial not null,
    idAvion bigint not null,
    dateDebut date not null,
    dateFin date not null     
);

INSERT INTO ASSURANCE VALUES 
(default, 1, '2022-11-15', '2022-12-15'),
(default, 2, '2022-11-15', '2023-01-15'),
(default, 3, '2022-11-15', '2023-02-15');

UPDATE avion SET imageavion = 'iVBORw0KGgoAAAANSUhEUgAABVYAAAMACAIAAABAXKuVAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4nOydd1hUx9rA3zll+8IuHUEFRKWLvcauMXZN1CS2tHtTb5rpud9NLzf9JiYmJmoSNUVjotHYW+wFsSCCIEhvC8v2es7M98eysMDSEY05v4dHd2fnvPPOzDu9HBTbJwkE/jKg662AgECXgigmMOkWxj8CaMn11kXgBoC3cZVXNWlHCOa6NmDStcHdKFyPaN/EzVyXJmer0xGxLMOwjM1qI6QrNPybliUBAQGBtnPtKkzmmkkW6ERu4i6RgEBzBCSMtCsj9CYOY+P11kXg+kNRSOITGZBANBf+7NqQPSvhv9EQ5npEuzacm6/hq41R1420W5OIPM9zHAeAukYxl0p/o1IkICAg0F6uXb0sTAHc4Nx8fSABgTbgkIVarPbrrYXAjQLmwWLlQRZ6XbX4u08HCHMBHabrRsGtmAggGBMPv12EMBEgICA…lkMplMjiefTqfOLoAqOg/HruumaXr2wPZtkD13RchkMplMJpPJZDKZXLt86A/zEYBBgDr6/efLy+vb/SxA9+hLL06Dsg88/ceRyWQymUwmk8lkMjmKfLlcT6fT5XyyDrCOflyHl9e3aZq6vuu7/tlje46OtiOTyWQymUwmk8lkco3yMAzzFYAf18EgQB0dj4cxXbqum27d1D2dCJAkSZIk6a+GYTifL13XjelyPB7cAqim6/k03aaff7xOt67ru/7pVQBJkiRJUusd+sNwPs///T/9dr6eT13XOQKoqd+uQ3/o//PzZZqmaeoul+vb29t0u92m2/xAdqrkw5HJZDKZTCaTyWQyuQp5XiX/10b5MV3mv/931gHW2Pv77b9//np5ffun/0UkSZIkSd+3y/n04zocj39vAHALoL6Ox8O/0uXHdXj99f7r1/vb++32/P8XKEmSJElqp0Pfn46HYTieh+P9X/7n/gfTfIZ0ARGD/wAAAABJRU5ErkJggg==', format = 'png' WHERE idavion = 1;


