package com.example.tpAvion.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class TokenNotFoundException extends Exception {
    public TokenNotFoundException(String a) {
        super(a);
    }
}
