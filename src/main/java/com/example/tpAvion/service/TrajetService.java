/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.tpAvion.service;

import com.example.tpAvion.model.Trajet;
import com.example.tpAvion.repository.TrajetRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrajetService {
    @Autowired
    private TrajetRepository trajetRepository;

    public Trajet saveTrajet(Trajet trajet) {
        Trajet savedTrajet = trajetRepository.save(trajet);
        return savedTrajet;
    }

    public Trajet updateTrajet(Trajet traj) {
        return trajetRepository.save(traj);
    }

    public void deleteTrajet(Integer id) {
        trajetRepository.deleteById(id);
    }
}
