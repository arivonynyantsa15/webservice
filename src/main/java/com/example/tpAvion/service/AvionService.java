package com.example.tpAvion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tpAvion.model.Avion;
import com.example.tpAvion.model.Trajet;
import com.example.tpAvion.model.receiver.AvionImageReceiver;
import com.example.tpAvion.model.Assurance;
import com.example.tpAvion.repository.AvionRepository;
import com.example.tpAvion.repository.AssuranceRepository;

@Service
public class AvionService {
    @Autowired
    private AvionRepository avionRepository;

    @Autowired
    private AssuranceRepository assuranceRepository;

    public Optional<Avion> getById(Integer id) {
        return avionRepository.findById(id);
    }

    public Avion save(Avion avion) {
        return avionRepository.save(avion);
    }

    public List<Avion> getAll() {
        return avionRepository.findAll();
    }

    public void delete(Avion avion) {
        avionRepository.delete(avion);
    }

    public Avion update(Integer id, Avion avion) {
        avion.setId(id);
        return avionRepository.save(avion);
    }

    public List<Trajet> getTrajets(Integer id) {
        return avionRepository.findById(id).get().getTrajets();
    }

    public List<Assurance> getAssuranceExpire(Integer num) {
        return assuranceRepository.getAssuranceExpires(num - 1, num);
    }

    public boolean updateImage(AvionImageReceiver image) {
        try{
            avionRepository.updateImage(image.getIdAvion(), image.getImage(), image.getFormat()) ;
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
