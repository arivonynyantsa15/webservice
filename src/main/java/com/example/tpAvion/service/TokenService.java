package com.example.tpAvion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tpAvion.exception.TokenNotFoundException;
import com.example.tpAvion.model.Connexion;
import com.example.tpAvion.repository.ConnexionRepository;

@Service
public class TokenService {

    @Autowired
    private ConnexionRepository connexionrepository;

    public void Auth(Integer idUser, String token) throws TokenNotFoundException {
        Connexion con = connexionrepository.getActiveConnexion(idUser);
        if (con == null || (!token.equals(con.getToken()))) {
            throw new TokenNotFoundException("Please relogin or logon");
        }
    }
}
