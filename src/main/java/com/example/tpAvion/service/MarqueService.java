package com.example.tpAvion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tpAvion.model.Marque;
import com.example.tpAvion.repository.MarqueRepository;

@Service
public class MarqueService {
    @Autowired
    private MarqueRepository marqueRepository;

    public List<Marque> getAll() {
        return marqueRepository.findAll();
    }
}
