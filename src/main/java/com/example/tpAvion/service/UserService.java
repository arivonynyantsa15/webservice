package com.example.tpAvion.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tpAvion.exception.NotFoundException;
import com.example.tpAvion.model.Connexion;
import com.example.tpAvion.model.User;
import com.example.tpAvion.repository.ConnexionRepository;
import com.example.tpAvion.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ConnexionRepository conRepo;

    public Connexion login(String email, String motDePasse) throws UnsupportedEncodingException, NotFoundException {
        User u = userRepo.findByEmailAndMotDePasse(email, motDePasse);
        if (u != null) {
            Connexion con = conRepo.getActiveConnexion(u.getIdUser());
            if (con != null) {
                return con;
            }
            con = new Connexion(email, motDePasse);
            con.setIdUser(u.getIdUser());
            conRepo.save(con);
            return con;
        } else {
            throw new NotFoundException("The user is not found");
        }
    }

    public void deconnect(Integer idUser) {
        Connexion connexion = conRepo.getActiveConnexion(idUser);
        if (connexion != null) {
            connexion.setExpiration(new Date());
            conRepo.save(connexion);
        }
    }
}
