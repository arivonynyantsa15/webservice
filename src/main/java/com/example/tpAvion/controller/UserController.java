package com.example.tpAvion.controller;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.tpAvion.exception.NotFoundException;
import com.example.tpAvion.model.Data;
import com.example.tpAvion.model.User;
import com.example.tpAvion.service.UserService;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    private UserService userServ;

    @PostMapping("/login")
    public Data login(@RequestBody User user) throws UnsupportedEncodingException, NotFoundException {
        System.out.println(user.getEmail() + " " + user.getMotDePasse());
        return new Data(userServ.login(user.getEmail(), user.getMotDePasse()));
    }

    @PutMapping("/deconnexion/{id}")
    public void deconnect(@PathVariable Integer id) {
        userServ.deconnect(id);
    }
}
