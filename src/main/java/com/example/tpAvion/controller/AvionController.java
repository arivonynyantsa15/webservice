package com.example.tpAvion.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.example.tpAvion.exception.TokenNotFoundException;
import com.example.tpAvion.model.Avion;
import com.example.tpAvion.model.Data;
import com.example.tpAvion.model.receiver.AvionImageReceiver;
import com.example.tpAvion.service.AvionService;
import com.example.tpAvion.service.TokenService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin
@RestController
public class AvionController {
    @Autowired
    private AvionService avionService;
    @Autowired
    private TokenService tokenService;

    @PostMapping(path = "/avions")
    public Avion create(@RequestBody Avion newAvion, @RequestParam String id, @RequestParam String token)
            throws NumberFormatException, TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(id), token);
        return avionService.save(newAvion);
    }

    @GetMapping(path = "/avions")
    public Data getAll() {
        return new Data(avionService.getAll());
    }

    @DeleteMapping(path = "/avions/{id}")
    public void delete(@PathVariable Integer id, @RequestParam String idU, @RequestParam String token)
            throws TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(idU), token);
        Optional<Avion> avionOptional = avionService.getById(id);
        if (avionOptional.isPresent()) {
            Avion avion = avionOptional.get();
            avionService.delete(avion);
        }

    }

    @PutMapping(path = "/avions/{id}")
    public Data update(@PathVariable Integer id, @RequestBody Avion avion, @RequestParam String idU,
            @RequestParam String token) throws TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(id), token);
        return new Data(avionService.update(id, avion));
    }

    @GetMapping(path = "/avions/{id}")
    public Data getById(@PathVariable Integer id, @RequestParam String idU, @RequestParam String token)
            throws NumberFormatException, TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(idU), token);
        return new Data(avionService.getById(id));
    }

    @GetMapping(path = "/avions/{id}/trajets")
    public Data getTrajets(@PathVariable Integer id, @RequestParam String idU, @RequestParam String token)
            throws TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(idU), token);
        return new Data(avionService.getTrajets(id));
    }

    @GetMapping(path = "/avions/assurance-expire/{num}")
    public Data getAssuranceExpire(@PathVariable("num") Integer num) throws TokenNotFoundException {
        return new Data(avionService.getAssuranceExpire(num));
    }

    @PutMapping(path = "/avions/image")
    public Data updateImage(@RequestBody AvionImageReceiver image){
        return new Data(avionService.updateImage(image));
    }
}
