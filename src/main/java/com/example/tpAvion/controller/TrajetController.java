/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.tpAvion.controller;

import com.example.tpAvion.exception.TokenNotFoundException;
import com.example.tpAvion.model.Data;
import com.example.tpAvion.model.Trajet;
import com.example.tpAvion.service.TokenService;
import com.example.tpAvion.service.TrajetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/trajets")
public class TrajetController {

    @Autowired
    private TrajetService trajetService;
    @Autowired
    private TokenService tokenService;

    @PostMapping("")
    public Data saveTrajet(@RequestBody Trajet traj, @RequestParam String token, @RequestParam String id)
            throws NumberFormatException, TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(id), token);
        return new Data(trajetService.saveTrajet(traj));
    }

    @PutMapping("/{id}")
    public Data updateTrajet(@PathVariable("id") Integer id, @RequestBody Trajet trajet, @RequestParam String token,
            @RequestParam String idU) throws NumberFormatException, TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(idU), token);
        trajet.setId(id);
        return new Data(trajetService.updateTrajet(trajet));
    }

    @DeleteMapping("/{id}")
    public void deleteTrajet(@PathVariable("id") Integer id, @RequestParam String token, @RequestParam String idU)
            throws TokenNotFoundException {
        tokenService.Auth(Integer.valueOf(idU), token);
        trajetService.deleteTrajet(id);
    }
}
