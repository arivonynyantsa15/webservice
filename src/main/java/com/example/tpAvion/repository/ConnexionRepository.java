package com.example.tpAvion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.tpAvion.model.Connexion;

public interface ConnexionRepository extends JpaRepository<Connexion, Integer> {
    @Query(value = "SELECT * FROM connexion WHERE iduser=?1 AND expiration > now()", nativeQuery = true)
    public Connexion getActiveConnexion(Integer idUser);
}
