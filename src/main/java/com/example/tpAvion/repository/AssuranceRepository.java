package com.example.tpAvion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.tpAvion.model.Assurance;

public interface AssuranceRepository extends JpaRepository<Assurance, Integer> {

    @Query(value = "SELECT * FROM assurance WHERE dateFin > now() + INTERVAL '?1 MONTH' and dateFin < now() + INTERVAL '?2 MONTH'", nativeQuery = true)
    public List<Assurance> getAssuranceExpires(int monthMin, int monthMax);
}
