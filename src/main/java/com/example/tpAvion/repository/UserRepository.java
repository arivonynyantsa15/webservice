package com.example.tpAvion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.tpAvion.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "select * from users u where u.email=?1 and u.motdepasse=md5(?2)", nativeQuery = true)
    public User findByEmailAndMotDePasse(String mail, String motDePasse);
}
