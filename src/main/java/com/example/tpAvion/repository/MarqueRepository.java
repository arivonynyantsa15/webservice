package com.example.tpAvion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.tpAvion.model.Marque;

@Repository
public interface MarqueRepository extends JpaRepository<Marque, Integer> {

}
