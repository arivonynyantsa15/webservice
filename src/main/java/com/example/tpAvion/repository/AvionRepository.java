package com.example.tpAvion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.tpAvion.model.Avion;

@Repository
public interface AvionRepository extends JpaRepository<Avion, Integer> {

    @Query(value = "UPDATE avion SET imageavion = ?2, format = ?3 WHERE idavion = ?1", nativeQuery = true)
    public void updateImage(Integer idAvion, String image, String format);
}
