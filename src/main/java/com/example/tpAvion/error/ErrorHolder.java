package com.example.tpAvion.error;

public class ErrorHolder {
    private Error error;

    public ErrorHolder(Error error) {
        this.error = error;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

}
