package com.example.tpAvion.error;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.tpAvion.exception.NotFoundException;
import com.example.tpAvion.exception.TokenNotFoundException;
import com.example.tpAvion.exception.WrongValueException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException exception) {
        Error error = new Error(404, HttpStatus.NOT_FOUND, exception.getMessage());
        return buildResponseEntity(error);
    }

    @ExceptionHandler(WrongValueException.class)
    protected ResponseEntity<Object> handleWrongValueException(WrongValueException e) {
        Error error = new Error(400, HttpStatus.BAD_REQUEST, e.getMessage());
        return buildResponseEntity(error);
    }

    @ExceptionHandler(TokenNotFoundException.class)
    protected ResponseEntity<Object> handleTokenNotFoundException(TokenNotFoundException e) {
        Error error = new Error(401, HttpStatus.UNAUTHORIZED, e.getMessage());
        return buildResponseEntity(error);
    }

    private ResponseEntity<Object> buildResponseEntity(Error error) {
        return new ResponseEntity<>(new ErrorHolder(error), error.getStatus());
    }
}
