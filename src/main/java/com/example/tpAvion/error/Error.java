package com.example.tpAvion.error;

import org.springframework.http.HttpStatus;

public class Error {
    private Integer code;
    private HttpStatus status;
    private String message;

    public Error(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Error(Integer code, HttpStatus status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

}
