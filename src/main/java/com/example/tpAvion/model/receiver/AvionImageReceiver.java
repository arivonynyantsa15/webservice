package com.example.tpAvion.model.receiver;

public class AvionImageReceiver {
    private Integer idAvion;
    private String image;
    private String format;

    public Integer getIdAvion() {
        return idAvion;
    }
    public void setIdAvion(Integer idAvion) {
        this.idAvion = idAvion;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getFormat() {
        return format;
    }
    public void setFormat(String format) {
        this.format = format;
    }
}
