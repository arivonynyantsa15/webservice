package com.example.tpAvion.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAlias;

@Entity
@Table(name = "avion")
public class Avion {
    @Id
    @Column(name = "idavion")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idmarque", referencedColumnName = "idmarque")
    private Marque marque;

    @JsonAlias({ "nImmatriculation", "n_immatriculation" })
    @Column(name = "nimmatriculation")
    private String nImmatriculation;

    @Column(name = "imageavion")
    private String image;

    @Column(name = "format")
    private String format;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @OneToMany
    @JoinColumn(name = "idavion")
    private List<Trajet> trajets;

    public Integer getId() {
        return id;
    }

    public String getnImmatriculation() {
        return nImmatriculation;
    }

    public void setnImmatriculation(String nImmatriculation) {
        this.nImmatriculation = nImmatriculation;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Trajet> getTrajets() {
        return trajets;
    }

    public void setTrajets(List<Trajet> trajets) {
        this.trajets = trajets;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

}
