/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.tpAvion.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Trajet")
public class Trajet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtrajet")
    private Integer id;

    @Column(name = "idavion")
    private Integer idAvion;

    @Column(name = "dateinsert")
    @Temporal(TemporalType.DATE)
    private Date dateTrajet;

    @Column(name = "debutkm")
    private int debutKm;

    @Column(name = "finkm")
    private int finKm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(Integer idAvion) {
        this.idAvion = idAvion;
    }

    public Date getDateTrajet() {
        return dateTrajet;
    }

    public void setDateTrajet(Date dateTrajet) {
        this.dateTrajet = dateTrajet;
    }

    public int getDebutKm() {
        return debutKm;
    }

    public void setDebutKm(int debutKm) {
        this.debutKm = debutKm;
    }

    public int getFinKm() {
        return finKm;
    }

    public void setFinKm(int finKm) {
        this.finKm = finKm;
    }

}
