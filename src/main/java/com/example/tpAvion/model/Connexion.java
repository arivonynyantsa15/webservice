package com.example.tpAvion.model;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Clock;
import java.util.Date;
import java.util.Formatter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Connexion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idconnexion")
    private Integer idConnexion;

    @Column(name = "iduser")
    private Integer idUser;

    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expiration;

    public Connexion() {
    }

    public Connexion(String email, String mdp) throws UnsupportedEncodingException {
        genToken(email, mdp);
        genDateExp();
    }

    private void genDateExp() {
        Date date = new Date();
        expiration = new Date(date.getTime() + 1000 * 60 * 60 * 24);
    }

    private void genToken(String email, String mdp) throws UnsupportedEncodingException {
        String rand = email + mdp + Clock.systemUTC().instant().toString();
        MessageDigest md = null;
        String sha1 = "";
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.reset();
            md.update(rand.getBytes("UTF-8"));
            sha1 = byteToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.token = sha1;
    }

    private String byteToHex(final byte[] hash) {
        Formatter format = new Formatter();
        for (byte b : hash) {
            format.format("%02x", b);
        }
        String result = format.toString();
        format.close();
        return result;
    }

    public Integer getIdConnexion() {
        return idConnexion;
    }

    public void setIdConnexion(Integer idConnexion) {
        this.idConnexion = idConnexion;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

}
